//Node.js Routing with HTTP Methods

//CRUD Operations		HTTP Methods
	//C - reate 		POST
	//R - ead 			GET
	//U - pdate 		PUT / PATCH
	//D - elete 		DELETE


const http = require('http');
const port = 4000;

//Mock data for users and courses
let users = [
	{
		userName : "peterIsHomeless",
		email : 'peterParker@mail.com',
		password : 'peterNoWayHome'
	},

	{
		userName : "Tony3000",
		email : 'starkIndustries@mail.com',
		password : 'ironManWillBeBack'
	}
];

let courses = [
	{
		name : 'Math 103',
		price : 2500,
		isActive : true
	},

	{
		name : 'Biology 201',
		price : 2500,
		isActive : true
	}
];

http.createServer((req, res) => {

	if(req.url === '/' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking GET Method');

	} else if(req.url === '/' && req.method === 'POST') {
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking POST Method');

	} else if(req.url === '/' && req.method === 'PUT') {
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking PUT Method');

	} else if(req.url === '/' && req.method === 'DELETE') {
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This is for checking DELETE Method');

	} else if(req.url === '/users' && req.method === 'GET') {
		//change the value of Content-Type header if we are passing json as our server
		res.writeHead(200, {'Content-Type' : 'application/json'});
		//we cannot pass other data type as a response except for a string
		//to be able to pass the array of users, first we stringify the array as JSON
		res.end(JSON.stringify(users));

	} else if(req.url === '/courses' && req.method === 'GET') {
		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(courses));

	} else if(req.url === '/users' && req.method === 'POST') {
		let requestBody = '';
		//receiving data from client to nodejs server requires 2 steps:
			//data step - this part will read the stream of data from our client and process the incoming data into the requestBody variable
		req.on('data', (data) => {
			console.log(data.toString());
			requestBody += data
		})
			//end step - will run once or after the request has been completely sent from our client
		req.on('end', () => {
			console.log(requestBody);
			requestBody = JSON.parse(requestBody)
			let newUser = {
				userName : requestBody.userName,
				email : requestBody.email,
				password : requestBody.password
			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(users));
		})

	} else if(req.url === '/courses' && req.method === 'POST') {
		let requestBody = '';
		req.on('data', (data) => {
			requestBody += data
		})
		req.on('end', () => {
			requestBody = JSON.parse(requestBody)
			let newCourse = {
				name : requestBody.name,
				price : requestBody.price,
				isActive : requestBody.isActive
			}

			courses.push(newCourse);

			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(courses));
		})
	}

}).listen(port)

console.log('Server is running on localhost: 4000');


//mini-activity
